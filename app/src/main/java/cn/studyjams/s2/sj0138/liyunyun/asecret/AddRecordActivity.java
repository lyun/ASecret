package cn.studyjams.s2.sj0138.liyunyun.asecret;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.DbAdapter;
import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.MyApp;
import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.Record;

public class AddRecordActivity extends AppCompatActivity {

    private ImageView back_img;
    private EditText edt_title,edt_password,edt_username,edt_phone,edt_email;
    private String title,password,username,phone,email;
    private Button btn_add_record;
    private int record_id;
    private DbAdapter dbAdapter;
    private MyApp myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_record);

        back_img = (ImageView) findViewById(R.id.back_img);
        edt_title = (EditText) findViewById(R.id.edt_title);
        edt_password = (EditText) findViewById(R.id.edt_password);
        edt_username = (EditText) findViewById(R.id.edt_username);
        edt_phone = (EditText) findViewById(R.id.edt_phone);
        edt_email = (EditText) findViewById(R.id.edt_email);
        btn_add_record = (Button) findViewById(R.id.btn_add_record);

        dbAdapter = new DbAdapter(this);

        back_img.setOnClickListener(new OnClickListenerImpl());
        btn_add_record.setOnClickListener(new OnClickListenerImpl());

        Intent intent = getIntent();
        record_id = intent.getIntExtra("record_id",-1);

        if(record_id>0)
            init();
    }

    private void init(){
        dbAdapter.open();
        Record record = dbAdapter.findRecord(record_id);
        edt_title.setText(record.getTitle());
        edt_password.setText(record.getPassword()+"");
        edt_username.setText(record.getUsername()+"");
        edt_phone.setText(record.getPhone()+"");
        edt_email.setText(record.getEmail()+"");
        dbAdapter.close();
    }

    private class OnClickListenerImpl implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.back_img:
                    finish();
                    break;
                case R.id.btn_add_record:
                    title = edt_title.getText().toString();
                    password = edt_password.getText().toString();
                    username = edt_username.getText().toString();
                    phone = edt_phone.getText().toString();
                    email = edt_email.getText().toString();
                    if(title.isEmpty()||password.isEmpty()){
                        Toast.makeText(getApplicationContext(),
                                "前两项不能为空",Toast.LENGTH_SHORT).show();
                    }else{
                        if(record_id>0)
                            updateRecord();
                        else
                            addRecord();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void updateRecord(){
        Record record = new Record();
        record.setRecord_id(record_id);
        record.setTitle(title);
        record.setPassword(password);
        record.setUsername(username);
        record.setPhone(phone);
        record.setEmail(email);
        dbAdapter.open();
        myApp = (MyApp) this.getApplication();
        //根据record_id更新数据库
        int result = dbAdapter.updateRecord(record);
        if(result>0){
            Toast.makeText(getApplicationContext(),"更新成功",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(AddRecordActivity.this,MainActivity.class));
            finish();
        }else{
            Toast.makeText(getApplicationContext(),"更新失败",Toast.LENGTH_SHORT).show();
        }
        dbAdapter.close();
    }

    private void addRecord(){
        Record record = new Record();
        record.setTitle(title);
        record.setPassword(password);
        record.setUsername(username);
        record.setPhone(phone);
        record.setEmail(email);

        dbAdapter.open();
        myApp = (MyApp) this.getApplication();
        int result = (int)dbAdapter.insertRecord(record);
        if(result>0) {
            record.setRecord_id(result);
            myApp.getList().add(record);
            startActivity(new Intent(AddRecordActivity.this,MainActivity.class));
            finish();
        }else{
            Toast.makeText(getApplicationContext(),"添加失败",Toast.LENGTH_SHORT).show();
        }

        dbAdapter.close();
    }
}
