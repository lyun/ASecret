package cn.studyjams.s2.sj0138.liyunyun.asecret;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.DbAdapter;
import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.MyApp;
import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.Record;
import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.User;

public class ConfigActivity extends AppCompatActivity {

    private EditText edt_password;
    private Button btn_config;
    private DbAdapter dbAdapter;
    private MyApp myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        edt_password = (EditText) findViewById(R.id.edt_pass);
        btn_config = (Button) findViewById(R.id.btn_config);

        btn_config.setOnClickListener(new OnClickListenerImpl());

        //数据库的初始化
        dbAdapter = new DbAdapter(this);
        dbAdapter.open();
        myApp = (MyApp) this.getApplication();
        User user = dbAdapter.findUser();
        List<Record> list = dbAdapter.getAllRecords();
        if(user != null) {
            myApp.setUser(user);
        }
        if(list != null){
            myApp.setList(list);
        }
        dbAdapter.close();

        //判断隐私模式是否打开
        if(user.getPattern()==0){
            startActivity(new Intent(ConfigActivity.this,MainActivity.class));
            finish();
        }
    }

    private class OnClickListenerImpl implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_config:
                    config();
                    break;
                default:
                    break;
            }
        }
    }

    private void config(){
        String password = edt_password.getText().toString();
        if(myApp.getUser().getPassword().equals(password)){
//            Toast.makeText(getApplicationContext(),"密码验证通过",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(ConfigActivity.this,MainActivity.class));
            finish();
        }else{
            Toast.makeText(getApplicationContext(),"密码验证失败",Toast.LENGTH_SHORT).show();
        }
    }
}
