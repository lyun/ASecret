package cn.studyjams.s2.sj0138.liyunyun.asecret;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.id.home;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;
//    private CircleImageView circleImageView;
    private FloatingActionButton fab;
    private TextView toolbarTitle;
    private long exitTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //获取toolbar
        Toolbar toolbar =(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //获取drawerlayout
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        //获取navigationview
        navigationView = (NavigationView)findViewById(R.id.nav_view);
        //获取FloatingActionButton
        fab = (FloatingActionButton)findViewById(R.id.float_btn);
        //获取toolbarTitle
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        //获取actionbar,显示并设置导航图标
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.nav);
        }

        //初始化页面
        replaceFragment(new MyListFragment());
        navigationView.setCheckedItem(R.id.nav_list);
        toolbarTitle.setText("我的列表");
        //设置navigationview
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener(){

            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch(item.getItemId()){
                    case R.id.nav_list:
                        fab.setVisibility(View.VISIBLE);
                        replaceFragment(new MyListFragment());
                        navigationView.setCheckedItem(R.id.nav_list);
                        toolbarTitle.setText("我的列表");
                        break;
                    case R.id.nav_settings:
                        fab.setVisibility(View.GONE);
                        replaceFragment(new SetingsFragment());
                        navigationView.setCheckedItem(R.id.nav_settings);
                        toolbarTitle.setText("隐私模式");
                        break;
                    case R.id.nav_contact:
                        fab.setVisibility(View.GONE);
                        replaceFragment(new ContactFragment());
                        navigationView.setCheckedItem(R.id.nav_contact);
                        toolbarTitle.setText("关于我们");
                        break;
                }
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
        //*悬浮按钮
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,AddRecordActivity.class);
                intent.putExtra("record_id",-1);
                startActivity(intent);
            }
        });
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_layout,fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            //导航键
            case home://注意加android
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if((System.currentTimeMillis()-exitTime) > 2000){
            Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
        }
        //return ;
    }
}
