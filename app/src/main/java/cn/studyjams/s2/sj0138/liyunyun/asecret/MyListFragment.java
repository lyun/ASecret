package cn.studyjams.s2.sj0138.liyunyun.asecret;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.DbAdapter;
import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.MyApp;
import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.Record;
import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.RecordAdapter;

public class MyListFragment extends Fragment {

    private Activity context;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView my_list;
//    private ImageView img_search;
//    private EditText edt_search;
    private MyApp myApp;
    private List<Record> recordList;
    private RecordAdapter recordAdapter;
    private DbAdapter dbAdapter;
    private int id,record_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_list,container,false);
        context = this.getActivity();

        dbAdapter = new DbAdapter(context);
        myApp = (MyApp) context.getApplication();
        recordList = myApp.getList();

//        img_search = (ImageView) view.findViewById(R.id.img_search);
//        edt_search = (EditText) view.findViewById(R.id.edt_search);
        my_list = (ListView)view.findViewById(R.id.my_list);
        this.recordAdapter = new RecordAdapter(context,recordList);
        my_list.setAdapter(recordAdapter);

        my_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent detailIntent = new Intent(context,AddRecordActivity.class);
                record_id = recordList.get(i).getRecord_id();
                detailIntent.putExtra("record_id",record_id);
                startActivity(detailIntent);
            }
        });

        my_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                id = i;
                record_id = recordList.get(i).getRecord_id();
                showDialog();
                return true;
            }
        });

//        img_search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                search();
////                Toast.makeText(context,edt_search.getText().toString(),Toast.LENGTH_SHORT).show();
//            }
//        });

        //*下拉刷新
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.list_swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                requestData();
            }
        });

        return view;
    }

    //从数据库获取列表
    private void requestData(){
        new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                context.runOnUiThread(new Runnable(){
                    @Override
                    public void run() {
                        dbAdapter.open();
                        recordList = dbAdapter.getAllRecords();
                        if(recordList != null){
                            myApp.setList(recordList);
                        }
                        dbAdapter.close();
                        recordAdapter.notifyDataSetChanged();
                        // 取消刷新效果
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        }).start();
    }

    private void deleteRecord(){
        dbAdapter.open();
        int result = dbAdapter.deleteRecord(record_id);
        if(result>0){
            requestData();
            recordList.remove(id);
            recordAdapter.notifyDataSetChanged();
        }else{
            Toast.makeText(context,"删除失败",Toast.LENGTH_SHORT).show();
        }
        dbAdapter.close();
    }

    private void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("确定删除该条记录？");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteRecord();
            }
        });
        builder.setNegativeButton("取消",null);
        AlertDialog dialog  = builder.create();
        dialog.show();
    }

    private void search(){
//        String search = edt_search.getText().toString();
        dbAdapter.open();
        recordList.clear();
//        recordList = dbAdapter.searchRecords(search);
        recordAdapter.notifyDataSetChanged();
        dbAdapter.close();
    }
}
