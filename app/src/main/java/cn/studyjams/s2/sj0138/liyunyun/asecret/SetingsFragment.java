package cn.studyjams.s2.sj0138.liyunyun.asecret;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.DbAdapter;
import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.MyApp;

import static android.app.Activity.RESULT_OK;

public class SetingsFragment extends Fragment {

    private Activity context;
    private ImageView img_secret,img_password;//img_info,
    private int secretOpen;//隐私模式的开关
    private DbAdapter dbAdapter;
    private MyApp myApp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setings,container,false);
        context = this.getActivity();

        //获取组件
//        img_info = (ImageView)view.findViewById(R.id.img_info);
        img_secret = (ImageView)view.findViewById(R.id.img_secret);
        img_password = (ImageView)view.findViewById(R.id.img_password);

        //绑定监听事件
//        img_info.setOnClickListener(new OnClickListenerImpl());
        img_secret.setOnClickListener(new OnClickListenerImpl());
        img_password.setOnClickListener(new OnClickListenerImpl());

        //获取用户设置的隐私模式
        myApp = (MyApp) context.getApplication();
        secretOpen = myApp.getUser().getPattern();
        if(secretOpen>0) {
            Glide.with(context.getApplicationContext()).
                    load(R.drawable.on).into(img_secret);
        }else{
            Glide.with(context.getApplicationContext()).
                    load(R.drawable.off).into(img_secret);
        }

        return view;
    }

    //监听事件
    private class OnClickListenerImpl implements View.OnClickListener{
        @Override
        public void onClick(View view) {
            switch (view.getId()){
//                case R.id.img_info:
//                    Toast.makeText(context, "暂不实现", Toast.LENGTH_SHORT).show();
//                    startActivity(new Intent(context,SetInfoActivity.class));
//                    break;
                case R.id.img_secret:
                    //或者可以弹出对话框输入密码来config
//                    Intent intent = new Intent(context,ConfigActivity.class);
//                    startActivityForResult(intent,1);
                    secretOpen = 1 - secretOpen;
                    updatePattern();
                    break;
                case R.id.img_password:
                    startActivity(new Intent(context,UpdatePasswordActivity.class));
                    break;
                default:
                    break;
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 1:
                if(resultCode == RESULT_OK){
                    secretOpen = 1 - secretOpen;
                    updatePattern();
                }
                break;
            default:
                break;
        }
    }

    private void updatePattern(){
        //先更新到数据库中
        dbAdapter = new DbAdapter(context);
        dbAdapter.open();
        dbAdapter.updatePattern(secretOpen);
        myApp.getUser().setPattern(secretOpen);
        if(secretOpen>0) {
            Glide.with(context.getApplicationContext()).
                    load(R.drawable.on).into(img_secret);
        }else{
            Glide.with(context.getApplicationContext()).
                    load(R.drawable.off).into(img_secret);
            Toast.makeText(context.getApplicationContext(),
                    "关闭隐私模式，请注意信息安全",Toast.LENGTH_SHORT).show();
        }
    }
}
