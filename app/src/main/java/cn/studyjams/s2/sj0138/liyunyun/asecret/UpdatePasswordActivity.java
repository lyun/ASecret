package cn.studyjams.s2.sj0138.liyunyun.asecret;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.DbAdapter;
import cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil.MyApp;

public class UpdatePasswordActivity extends AppCompatActivity {

    private EditText edt_old_password,edt_new_password,edt_again_password;
    private String old_password,new_password,again_password;
    private Button update_btn;
    private ImageView back_img;
    private DbAdapter dbAdapter;
    private MyApp myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);

        //获取组件
        edt_old_password = (EditText) findViewById(R.id.edt_old_password);
        edt_new_password = (EditText) findViewById(R.id.edt_new_password);
        edt_again_password = (EditText) findViewById(R.id.edt_again_password);
        update_btn = (Button) findViewById(R.id.update_btn);
        back_img = (ImageView) findViewById(R.id.back_img);

        update_btn.setOnClickListener(new OnClickListenerImpl());
        back_img.setOnClickListener(new OnClickListenerImpl());

        myApp = (MyApp) this.getApplication();
    }

    private class OnClickListenerImpl implements View.OnClickListener{
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.update_btn:
                    old_password = edt_old_password.getText().toString();
                    new_password = edt_new_password.getText().toString();
                    again_password = edt_again_password.getText().toString();
                    if(!old_password.isEmpty()&&!new_password.isEmpty()&&!again_password.isEmpty()) {
                        if(new_password.equals(again_password))
                            updatePass();
                        else
                            Toast.makeText(getApplication(),"请两次输入相同的密码",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplication(),"请检查是否存在未填项",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.back_img:
                    finish();
                    break;
                default:
                    break;
            }
        }
    }

    private void updatePass(){
        //检查旧密码的正确性
        if(myApp.getUser().getPassword().equals(old_password)){
            dbAdapter = new DbAdapter(this);
            dbAdapter.open();
            //更新密码
            dbAdapter.updatePassword(new_password);
            myApp.getUser().setPassword(new_password);
            dbAdapter.close();
            finish();//回到之前的界面
        }else{
            Toast.makeText(getApplication(),"请输入正确的旧密码",Toast.LENGTH_SHORT).show();
        }
    }
}
