package cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Administrator on 2017/5/2.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "secret.db";//数据库的名字
    private static final int DB_VERSION=1;//数据库版本

    public DBHelper(Context context){
        //Context context, String name, CursorFactory factory, int version
        //第三个参数CursorFactory指定在执行查询时获得一个游标实例的工厂类,设置为null,使用系统默认的工厂类
        super(context, DB_NAME, null, DB_VERSION);
        Log.d("DBHelper","DBHelper");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //用于初次使用软件时生成数据库表
        //创建表info,list
        sqLiteDatabase.execSQL("create table user(_id integer primary key," +
                "password text,pattern integer)");
        sqLiteDatabase.execSQL("create table list(_id integer primary key autoincrement," +
                "title text,password text,username text,phone text,email text)");
        sqLiteDatabase.execSQL("insert into user(password,pattern) values(123456,0)");
        Log.d("Create Table","info,list");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //用于升级软件时更新数据库表结构
        Log.d("database upgrade",i+","+i1);
    }
}
