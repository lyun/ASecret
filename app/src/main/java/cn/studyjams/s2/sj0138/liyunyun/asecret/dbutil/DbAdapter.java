package cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/5/2.
 */

public class DbAdapter {

    private final Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public DbAdapter(Context _cContext) {
        this.context = _cContext;
    }

    public void close() {
        if(db!=null)
        {
            db.close();
            db=null;
        }
    }

    public void open() throws SQLiteException {
        dbHelper = new DBHelper(context);
        try{
            db = dbHelper.getWritableDatabase();
        }catch(Exception e)
        {
            db  = dbHelper.getReadableDatabase();
        }
    }

    public User findUser(){
        Cursor cursor = db.rawQuery("select * from user", null);
        if(cursor.moveToNext()) {
            Log.d("select from user",cursor.getInt(0)+","
                    +cursor.getString(1)+","+cursor.getInt(2) );
            User user = new User();
            user.setUser_id(cursor.getInt(0));
            user.setPassword(cursor.getString(1));
            user.setPattern(cursor.getInt(2));
//            Toast.makeText(context,cursor.getInt(0)+","
//                    +cursor.getString(1)+","+cursor.getInt(2),Toast.LENGTH_SHORT).show();
            return user;
        }
        cursor.close();
        return null;
    }

    public void updatePattern(int pattern){
        db.execSQL("update user set pattern = ? where _id = 1",new Object[]{pattern});
    }

    public void updatePassword(String newPass){
        db.execSQL("update user set password = ? where _id = 1",new Object[]{newPass});
    }

    public List<Record> getAllRecords(){
        Cursor cursor = db.rawQuery("select * from list", null);
        List<Record> recordList = new ArrayList<Record>();
        while (cursor.moveToNext()){
            Record record = new Record();
            record.setRecord_id(cursor.getInt(0));
            record.setTitle(cursor.getString(1));
            record.setPassword(cursor.getString(2));
            record.setUsername(cursor.getString(3));
            record.setPhone(cursor.getString(4));
            record.setEmail(cursor.getString(5));
            recordList.add(record);
        }
//        Toast.makeText(context,recordList.size()+"",Toast.LENGTH_SHORT).show();
        return recordList;
    }

    public List<Record> searchRecords(String title){
        Cursor cursor = db.rawQuery("select * from list where title like ?",
                new String[]{"%"+title+"%"});
        List<Record> recordList = new ArrayList<Record>();
        while (cursor.moveToNext()){
            Record record = new Record();
            record.setRecord_id(cursor.getInt(0));
            record.setTitle(cursor.getString(1));
            record.setPassword(cursor.getString(2));
            record.setUsername(cursor.getString(3));
            record.setPhone(cursor.getString(4));
            record.setEmail(cursor.getString(5));
            recordList.add(record);
        }
        return recordList;
    }

    public long insertRecord(Record record){
        ContentValues cv = new ContentValues();
        cv.put("title",record.getTitle());
        cv.put("password",record.getPassword());
        cv.put("username",record.getUsername());
        cv.put("phone",record.getPhone());
        cv.put("email",record.getEmail());
        return db.insert("list",null,cv);
    }

    public Record findRecord(int record_id){
        Log.d("select by id","1");
        Record record = new Record();
        Cursor cursor = db.rawQuery("select * from list where _id = "+record_id,null);
        if (cursor.moveToNext()){
            record.setRecord_id(cursor.getInt(0));
            record.setTitle(cursor.getString(1));
            record.setPassword(cursor.getString(2));
            record.setUsername(cursor.getString(3));
            record.setPhone(cursor.getString(4));
            record.setEmail(cursor.getString(5));
            Log.d("select by id",record.getTitle());
        }
        cursor.close();
        return record;
    }

    public int updateRecord(Record record){
        ContentValues cv = new ContentValues();
        cv.put("title",record.getTitle());
        cv.put("password",record.getPassword());
        cv.put("username",record.getUsername());
        cv.put("phone",record.getPhone());
        cv.put("email",record.getEmail());
        return db.update("list",cv,"_id=?",new String[]{record.getRecord_id()+""});
    }

    public int deleteRecord(int id){
        return db.delete("list","_id=?",new String[]{id+""});
    }
}
