package cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil;

import android.app.Application;

import java.util.List;

/**
 * Created by Administrator on 2017/5/2.
 */

public class MyApp extends Application {
    private User user;
    private List<Record> list;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Record> getList() {
        return list;
    }

    public void setList(List<Record> list) {
        this.list = list;
    }
}
