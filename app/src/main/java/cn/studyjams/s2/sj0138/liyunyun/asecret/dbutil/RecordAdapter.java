package cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cn.studyjams.s2.sj0138.liyunyun.asecret.R;

/**
 * Created by Administrator on 2017/5/2.
 */

public class RecordAdapter extends BaseAdapter {

    private Context context;
    private List<Record> recordList;
    private LayoutInflater layout;

    public RecordAdapter(Context context,List<Record> recordList){
        this.context = context;
        this.recordList = recordList;
        this.layout = LayoutInflater.from(context);
    }

    public class ViewHolder {
        public TextView txt_item_name;
    }

    @Override
    public int getCount() {
        return recordList.size();
    }

    @Override
    public Object getItem(int i) {
        return recordList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        //获取项目布局
        View v = view;
        final RecordAdapter.ViewHolder viewHolder = new RecordAdapter.ViewHolder();
        if(v == null)
            v = layout.inflate(R.layout.my_item, null);
        viewHolder.txt_item_name = (TextView) v.findViewById(R.id.txt_item_name);
        v.setTag(viewHolder);
        Record record = recordList.get(i);
        viewHolder.txt_item_name.setText(record.getTitle());
        return v;
    }
}
