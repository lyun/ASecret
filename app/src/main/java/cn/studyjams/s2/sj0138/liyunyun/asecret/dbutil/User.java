package cn.studyjams.s2.sj0138.liyunyun.asecret.dbutil;

/**
 * Created by Administrator on 2017/5/2.
 */

public class User {

    private int user_id;
    private String password;
    private int pattern;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPattern() {
        return pattern;
    }

    public void setPattern(int pattern) {
        this.pattern = pattern;
    }
}
